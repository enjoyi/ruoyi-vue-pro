#!/bin/bash
#set -e

DATE=$(date +%Y%m%d%H%M%S)
# 服务名称
APP_NAME=$1
# 模块名称
MODULE_NAME=$2
PORT=$3
# 基础路径
BUILD_BASE_PATH=/var/docker_mount/jenkins/build_workspace/$APP_NAME/$MODULE_NAME
# 编译后 jar 的地址。部署时，Jenkins 会上传 jar 包到该目录下
BASE_PATH=/var/docker_mount/jenkins/workspace/$APP_NAME
#// 应用部署路径
#        APP_DEPLOY_BASE_DIR = '/var/jenkins_home/build_workspace/'
# 环境
PROFILES_ACTIVE=development

# JVM 参数
JAVA_OPS="-Xms512m -Xmx512m -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=$HEAP_ERROR_PATH"


mkdir -p $BUILD_BASE_PATH

cp -f $BASE_PATH/$MODULE_NAME/target/$MODULE_NAME.jar $BUILD_BASE_PATH

cp -f $BASE_PATH/$MODULE_NAME/Dockerfile $BUILD_BASE_PATH

image_name=192.168.167.141:5000/${MODULE_NAME}:$DATE
docker build -t $image_name .
if [ $? -eq 0 ];then
	echo "docker build image $image_name success"
	rm -rf $BUILD_BASE_PATH
	echo "remove docker path!"
else
	echo "docker build image $image_name fail"
	exit 1
fi

echo "begin docker push $image_name"
docker push $image_name
echo "Clear docker $image_name"
docker rmi $image_name

echo "begin update container"
docker -H tcp://192.168.167.140:2375 stop $MODULE_NAME
docker -H tcp://192.168.167.140:2375 rm $MODULE_NAME
docker -H tcp://192.168.167.140:2375 run -idt --name $MODULE_NAME --restart=always -p $PORT:$PORT $image_name
if [ $? -eq 0 ];then
	echo "dokcer run suceess"
else
	echo "docker run fail"
fi
